package com.wordcount.wordcount.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wordcount.wordcount.domain.Word;

@Repository
public interface WordRepository extends JpaRepository<Word, String>{

}
