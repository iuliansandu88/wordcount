package com.wordcount.wordcount.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wordcount.wordcount.domain.Text;

@Repository
public interface TextRepository extends JpaRepository<Text, String> {

}
