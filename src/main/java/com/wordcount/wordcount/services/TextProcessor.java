package com.wordcount.wordcount.services;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wordcount.wordcount.domain.Word;
import com.wordcount.wordcount.repository.TextRepository;
import com.wordcount.wordcount.repository.WordRepository;

@Service
public class TextProcessor {
	
	@Autowired
	private WordRepository wRepository;
	
	@Autowired
	private TextRepository tRepository;
	
	static Map<String, Integer> map = new HashMap<String, Integer>();
	static List<String> words = new ArrayList<String>(); 
	
	public void work(String mode) {
		
		//Mode 1: read from a text file:
		if(mode.equals("FILE")) {
			String source = ".\\src\\main\\resources\\file.txt";
			String encoding = "UTF-8";
			BufferedReader reader = null;
			try 
			{
			    reader = new BufferedReader(new InputStreamReader(new FileInputStream(source), encoding));
			    for (String line; (line = reader.readLine()) != null;) {
			    		this.process(line);
			        }
			}
			catch(IOException e) {
				System.out.println(e);
			}		
			this.printResult(mode);
		}
		
		//Mode 2: read from a database table:
		if(mode.equals("DB")) {
			this.tRepository.findAll().stream().filter(line -> !line.toString().isEmpty()).forEach(text -> this.process(text.getText()));
			this.printResult(mode);
		}
	}
	
	public void process(String text) {
		words = Arrays.asList(text.split(" "));		
		words.forEach(word -> {
			word = word.replaceAll("[^A-Za-z]+", "").toLowerCase();
			if(map.containsKey(word))
				map.put(word, map.get(word) + 1);
			else
				map.put(word, 1);
		});		
	}
	
	public void printResult(String mode) {

		List<Word> list = map.entrySet().stream().map(Word::new).collect(Collectors.toList());		
        list.sort(Comparator.comparing(Word::getCount).reversed().thenComparing(Word::getWord));
        
		if(mode.equals("FILE")) {
			list.stream().forEach(word -> {
	            System.out.println(word.getWord() + " " + word.getCount());
	        });
		}
		if(mode.equals("DB")) {
			this.wRepository.deleteAll();
			list.stream().forEach(word -> {
				this.wRepository.save(word); 
	        });
		}
	}
}
