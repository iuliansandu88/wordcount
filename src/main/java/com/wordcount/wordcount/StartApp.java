package com.wordcount.wordcount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.wordcount.wordcount.services.TextProcessor;

@Component
public class StartApp implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private TextProcessor tpService;

    public void onApplicationEvent(final ApplicationReadyEvent event) {
    	    	
    	this.tpService.work("FILE");
    	this.tpService.work("DB");
        return;
    }
}
