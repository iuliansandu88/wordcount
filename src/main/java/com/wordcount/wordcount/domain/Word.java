package com.wordcount.wordcount.domain;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "words")
public class Word {
	
	@Id
	String word;
	int count;
	
	public Word(Map.Entry<String, Integer> entry) {
        this.word = entry.getKey();
        this.count = entry.getValue();
    }
	
	public String getWord() {
		return word;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}	
}
